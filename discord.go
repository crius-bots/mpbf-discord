package mpbf_discord

import (
	"context"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ponkey364/mpbf"
)

type DiscordRes struct {
	session *discordgo.Session
}

func (r *DiscordRes) Reply(message interface{}, to *mpbf.Request) (*mpbf.Message, error) {
	toMsg := to.Context.Value("discord.message").(*discordgo.Message)

	content, ok := message.(string)
	if !ok {
		return nil, nil
	}

	dMsg, err := r.session.ChannelMessageSendReply(toMsg.ChannelID, content, toMsg.Reference())
	if err != nil {
		return nil, err
	}

	return &mpbf.Message{
		Content:   content,
		MessageID: dMsg.ID,
		Realm: &mpbf.Realm{
			ID: dMsg.GuildID,
		},
		Author: &mpbf.User{
			ID: dMsg.Author.ID,
		},
	}, nil
}

func (r *DiscordRes) Send(message interface{}, to string) (*mpbf.Message, error) {
	content, ok := message.(string)
	if !ok {
		return nil, nil
	}

	dMsg, err := r.session.ChannelMessageSend(to, content)
	if err != nil {
		return nil, err
	}

	return &mpbf.Message{
		Content:   content,
		MessageID: dMsg.ID,
		Realm: &mpbf.Realm{
			ID: dMsg.GuildID,
		},
		Author: &mpbf.User{
			ID: dMsg.Author.ID,
		},
	}, nil
}

const PlatformName = "discord"

type DiscordPlatform struct {
	session *discordgo.Session
}

func (d *DiscordPlatform) Open(mc *mpbf.BotCommander, onMessage mpbf.CommandHandler) error {
	err := d.session.Open()
	if err != nil {
		return err
	}

	d.session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID {
			// ignore messages from us
			return
		}

		guild, err := s.Guild(m.GuildID)
		if err != nil {
			return
		}

		msg := &mpbf.Message{
			Content: m.Content,
			Author: &mpbf.User{
				ID:   m.Author.ID,
				Name: m.Author.Username,
			},
			Realm: &mpbf.Realm{
				ID:   m.GuildID,
				Name: guild.Name,
			},
		}

		ctx := context.WithValue(context.Background(), "discord.message", m.Message)
		ctx = context.WithValue(ctx, "platform", PlatformName)

		onMessage(&mpbf.Request{Message: msg, Context: ctx}, &DiscordRes{session: d.session})
	})

	return nil
}

func (d *DiscordPlatform) Close() error {
	return d.session.Close()
}

func (d *DiscordPlatform) GetPlatformType() string {
	return PlatformName
}

func (d *DiscordPlatform) JoinRealm(realmID string) error {
	return nil
}

func (d *DiscordPlatform) LeaveRealm(realmID string) error {
	return nil
}

func (d *DiscordPlatform) GetSession() interface{} {
	return d.session
}

func (d *DiscordPlatform) GetArgParser() func(rawArgs []string, command *mpbf.Command) ([]interface{}, error) {
	return mpbf.DefaultArgParser
}

func New(token string) (mpbf.Platform, error) {
	session, err := discordgo.New("Bot " + token)
	if err != nil {
		return nil, err
	}

	return &DiscordPlatform{
		session: session,
	}, nil
}
